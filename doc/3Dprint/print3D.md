# 3D print

## base know
* FDM printer can use 尼龙，树脂，ABS，TPU，PUA PPEK？
### FDM Design rule:
* 1 Degree to horizon more than 45 
* 2 Bridge distance<25mm
* 3 Pole bigger than 0.8mm


## 软件安装
* 软件生成3D打印文件，在slice软件cuba中配置的过程 ![](https://gitlab.com/0505/fablab/uploads/f401437a51056c0d0919fd4316d6b2e7/20200401191654.png)
* ![](https://gitlab.com/0505/fablab/uploads/6d7281977236389094d4a8a36db430f1/20200401191816.png)
* ![](https://gitlab.com/0505/fablab/uploads/f09b52c3aedaef72084fa6b0d8958242/20200401192225.png)
* ![](https://gitlab.com/0505/fablab/uploads/63537792c6f06e27bbe9a0910ac0273f/20200401193439.png)
* ![](https://gitlab.com/0505/fablab/uploads/44d4984e68c045c20f59f8eab23b5c5b/20200401193511.png)
* ![](https://gitlab.com/0505/fablab/uploads/9d58dc3a1c0e7f489335aba7ad3b3a8a/20200401193622.png)
* ![](https://gitlab.com/0505/fablab/uploads/b4f19ea05fd41bbe5454c03260674f8c/20200401194938.png)
* ![](https://gitlab.com/0505/fablab/uploads/d9b04a3711b53d6fe36a37c65131a0b2/20200401195354.png)
* ![](https://gitlab.com/0505/fablab/uploads/f2be4d5582cfed71275c7cceec87545d/20200401200156.png)
  
## 生成3D打印文件并设置相关参数
* ![](https://gitlab.com/0505/fablab/uploads/a2af80d41610e4bf846e16736f5c61f6/20200403161353.png)
* 保存文件![](https://gitlab.com/0505/fablab/uploads/5411ef283d561283c43d46b9bdc9e079/20200403162309.png)