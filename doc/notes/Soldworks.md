# Soldworks 学习笔记之零件制作篇

## 3D打印
* 将文件存为STL格式，然后导入cora进行打印
  

## 零件1
* 如下图![](https://gitlab.com/0505/fablab/uploads/56f5ce6c59b0730f24f563bcafc6dd10/20200405103000.png)
* 主件需要使用上视基准面，在偏移参考基准面的时候需要上移，俯视图画圆，否则可能会被底面的圆覆盖。导致不能封闭。
* 打孔选择技巧，鼠标移动到弧度可以显示出圆心，方便选择位置
* 鼠标左键弹出菜单可以编辑草图和编辑特征进行修改
* 拉伸切除可以不用管长度
## 扳手
* 最终图如下![](https://gitlab.com/0505/fablab/uploads/18c9dd9167447234c866ef3a835898b4/20200405104812.png)
* 12角梅花采用2个六边形交错剪切即可![](https://gitlab.com/0505/fablab/uploads/60b5b139626f6955d797187a6cf6ea65/20200405190249.png)
* 最麻烦的地方在扳手这边，在倒圆角的时候会把原有的线条给去除![](https://gitlab.com/0505/fablab/uploads/040ccdd2a53db00d62e3031343028c95/20200405190816.png)
* 做下边这个的圆倒角会将椭圆剪切掉，而且无法延伸出来，延伸出来的会无法固定。所以采用的是重画椭圆，需要固定椭圆。再重做一次发行可以延伸出来，原因估计是因为定位线一直没有用于参考的原因，先从上面往下延伸![](https://gitlab.com/0505/fablab/uploads/123cc4d81ecfb799f9602f0c07377284/20200405191032.png)
* 再往上延伸![](https://gitlab.com/0505/fablab/uploads/6ea55f5eb9b79ff8870795ae91294b4c/20200405191114.png)
* 最后固定一下切线![](https://gitlab.com/0505/fablab/uploads/ade12bcf9fb3738cef67b6d25b5d48de/20200405191218.png)
* 关于裁剪拉伸的必要项目是不能在原草图上操作，一定要在新的草图上操作，一旦操作就相当于持续性操作，直到你退出草图![](https://gitlab.com/0505/fablab/uploads/142f1efb332c743f835cc1fe59071133/20200405194146.png)
* 等距实体![](https://gitlab.com/0505/fablab/uploads/f62c81b104e553d9f88068680ff54b65/20200405202854.png)
* 如果在新草图中看不到原来草图的参考线，在草图1中显示出来即可![](https://gitlab.com/0505/fablab/uploads/c3285c5968ae369ab543a686bc40a61a/20200405202808.png)
* 操作完毕，退出草图![](https://gitlab.com/0505/fablab/uploads/cf6c85e24e7798a0193f950205e1dc39/20200405203050.png)
* 这才是最终裁剪拉伸的结束位置，设置凹陷的深度![](https://gitlab.com/0505/fablab/uploads/9d1d3fb9117b99aa80efa63a1aaace6f/20200405203111.png)
* 镜像操作的步骤是2个，第一个选择面，点击零件树，选择草图所在的面，我这是“前视基准面”![](https://gitlab.com/0505/fablab/uploads/d7c4b98a211f50e42e0f54eb045d59a0/20200405203713.png)
* 镜像避免重画一次同样的东西![](https://gitlab.com/0505/fablab/uploads/d620e1da2ef74887c7007491a900eaa2/20200405203741.png)
* over
## 拨叉
* ![](https://gitlab.com/0505/fablab/uploads/0c5505dd3b83bd84a9ecb191e55f1d90/20200405132528.png)
* 在连续连段这个位置软件会自动定位切圆并且固化![](https://gitlab.com/0505/fablab/uploads/7b35dedc33d7a11df62db31200341e18/20200405130001.png)
* 所以需要取消一次，分为2段直线，才能随动![](https://gitlab.com/0505/fablab/uploads/5c15f9c176a9a549c2ef1b4b680504b1/20200405130043.png)
* 参考线的蓝点不用管它![](https://gitlab.com/0505/fablab/uploads/9aeeffebbc5b52635c34ea51fd15bf57/20200405130340.png)
* over

## 钩子
* ![](https://gitlab.com/0505/fablab/uploads/339fefae43c2db533e9141303b3786c3/20200405141809.png)
* 如果已经删除的线段，可以延伸出来以免过多的回退
* 自动延伸技巧![](https://gitlab.com/0505/fablab/uploads/230d55d08e3a3b86171dbf8bc95239e1/20200405134309.png)
* 还是需要水平固定![](https://gitlab.com/0505/fablab/uploads/c6bea3e55e17e75a3cd23f00413343fd/20200405134647.png)
* 如果剪切不了近端，可能是因为没有重合，注意检查重合!![](https://gitlab.com/0505/fablab/uploads/111841722558f67f9bad62d2a9667749/20200405135259.png)
[](https://gitlab.com/0505/fablab/uploads/7b76068a5cc8024622d81c65dcc1bf0d/20200405135054.png)
* 要剪掉先，否则无法倒角![](https://gitlab.com/0505/fablab/uploads/274e9ec326bfa8b331f589b8c1c8cc86/20200405135515.png)
* 倒角后变蓝色，固定即可![](https://gitlab.com/0505/fablab/uploads/ca5c5b693c8b11f68a8ff292241c7b6c/20200405140024.png)![](https://gitlab.com/0505/fablab/uploads/f2b7f05b51b97f5f881e1da8c5ed9e2d/20200405140321.png) 
* 旋转必须是实线![](https://gitlab.com/0505/fablab/uploads/c9a334dea5d11105a435a15703411544/20200405140723.png)
* 圆角的面用法![](https://gitlab.com/0505/fablab/uploads/cc66ae79661e6cfb26182715844c4a0b/20200405141721.png)
* over
## 零件x
* ![](https://gitlab.com/0505/fablab/uploads/732672f80cc813ff824bc60dddb64773/20200405160628.png)
* 固定参考线![](https://gitlab.com/0505/fablab/uploads/8d09cebb91cc19878de4de6eb790633c/20200405145820.png)
* 参考面的设定，A 线点击线B再点击点可以获得垂直于参考线的参考面![](https://gitlab.com/0505/fablab/uploads/b67b0b590197c855761b7679066c7f53/20200405150031.png)
* 因为视野遮挡关系，正视图如果无法选取则旋转视图可以方便选择的角度画圆![](https://gitlab.com/0505/fablab/uploads/97f19a695188602492ff1a36f4bd59ae/20200405150812.png)
* 由于初始面选择的问题，导致只能这个角度，如何旋转90度是个问题？![](https://gitlab.com/0505/fablab/uploads/a507c176e8cb0adb481f2153adfdaf40/20200405151727.png)
* 切线的起点和终点不能是自动锚定点，需要偏开，看到自动切线和重合符号才放下![](https://gitlab.com/0505/fablab/uploads/90720dfd955a812c86e6277893eacffd/20200405151807.png)
* 这个地方卡了很久，是因为在已经拉伸的草图上再做草图拉伸，是无法再次拉伸的，退出草图会导致已经拉伸的图被切除![](https://gitlab.com/0505/fablab/uploads/9bf0d6df12594a624d48553bb3e010bb/20200405155409.png)
* 原则是：选择拉伸，创建草图，点击参考面，再画草图，如图会新增一个草图和拉伸![](https://gitlab.com/0505/fablab/uploads/3fe86bf70778709047c96d0a91edab95/20200405155504.png)
* 拉伸到一面![](https://gitlab.com/0505/fablab/uploads/e4c57818adc5fbbdaffffc43916e5863/20200405155548.png)
* 选择内切面![](https://gitlab.com/0505/fablab/uploads/48eaafa66792c63a6efd530a4b7fc92b/20200405155611.png)
* 最后是再做一次拉伸切除![](https://gitlab.com/0505/fablab/uploads/7259751f44b502db7827366e8e8ed409/20200405155926.png)
* over
  
## 零件X1 学习螺旋扫略
* 工程图![](https://gitlab.com/0505/fablab/uploads/f3a2fd38ecffb68e92f4ec10d3412738/d51c83f93f1c115d8cc3ca66381aed8.png)
* 最终图如下![](https://gitlab.com/0505/fablab/uploads/fb6ac0b121aec34074268828579df01a/20200410203926.png)
* 开始没有使用中心矩形，导致需要偏差参考面和多个尺寸![](https://gitlab.com/0505/fablab/uploads/5d85752edc8efa7fd7f1f3dbad9f6481/20200409202515.png)
* 第二个图示螺丝，![](https://gitlab.com/0505/fablab/uploads/c90c2bdf409ebee254487467526dbb4b/17aa867f21f33ef24c75622758c91f0.jpg)
* 螺旋高度和实物不符![](https://gitlab.com/0505/fablab/uploads/dbbe1e8e7b5938779745f0becc6b3eeb/20200410211806.png)
* 最后用了这个螺距![](https://gitlab.com/0505/fablab/uploads/4bc9d3db221fca9e3559922530f10b6e/20200410211907.png)
* 最终在这个地方软件崩溃了，这里的难点在这个半圈切除必须准确找到面，设计尺寸，剪切，旋转切除![](https://gitlab.com/0505/fablab/uploads/71f92eba6ee8f5d2d15c3a773f421a2b/20200410212202.png)
* over

## 零件X2 筋和镜像实体
* 第一个图![](https://gitlab.com/0505/fablab/uploads/6c57931f6cd1425198f45ef399438cda/20200412201836.png)
* 基准面的寻找需要用这2个面进行参考![](https://gitlab.com/0505/fablab/uploads/14448f907739810ee7d522b82afba655/20200412195613.png)
* 转换实体引用，就是选择一个实体或者线作为实体使用。找到圆，水平线![](https://gitlab.com/0505/fablab/uploads/dc65d5af37789cd926ca1b6f8fc6dfc7/20200412200712.png)
* 再找到侧视图的立柱![](https://gitlab.com/0505/fablab/uploads/495a9cb51206789f9cc936f6971bd738/20200412200859.png)
* 通过这2条线来固定尺寸![](https://gitlab.com/0505/fablab/uploads/4650ae308d60808846499bb815aad846/20200412201614.png)
* 设置参考线![](https://gitlab.com/0505/fablab/uploads/3e5e1a2b0264977d473148ee2dd6b81c/20200412201719.png)
* 第二个图![](https://gitlab.com/0505/fablab/uploads/05305553e57c49be4b8fba94144d69f6/20200412213511.png)
* 画一条线用于做参考面的第二参考，第一参考是面，然后选择线第二参考，修改角度![](https://gitlab.com/0505/fablab/uploads/172c4210cbd8b01dcbc2ea38935f9869/20200412211107.png)
* 这里选错面了，最后才发现居然涨了尾巴![](https://gitlab.com/0505/fablab/uploads/227e531f5dbd4d37828904fe19fbad7f/20200412212252.png)
* 选择拉伸到上面的这个面![](https://gitlab.com/0505/fablab/uploads/547d5f7a49ef2e4ed63b7c8f4abadb36/20200412212451.png)
* over