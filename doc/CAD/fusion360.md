# Lesson two

## 1 如何从现有的零件中修改后获得新的零件
* 拷贝零件
* 新建草图
* 拉伸


## 2 如何分享3D图纸
* 在fusion360中点击版本，点击web查看
![](https://gitlab.com/0505/fablab/uploads/c3be76654b749eb644ca2cd5a86d94a6/20200331162329.png)
* 在网页中点击分享
![](https://gitlab.com/0505/fablab/uploads/c9ad11cde8df2b3af2101d75b5e15f86/20200331164631.png)
* 选择嵌入，复制连接![](https://gitlab.com/0505/fablab/uploads/f49fc8195095db77b89f1284fb9478ad/20200331164735.png)

* 将嵌入式连接粘贴
<iframe src="https://nexpcb3.autodesk360.com/shares/public/SH919a0QTf3c32634dcf97c1103ebc7b646a?mode=embed" width="1024" height="768" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

* 在360中预览非常慢，在网页中预览也非常慢

## 3 为双目项目做一个摄像头固定支架
* 开始做了一个立方体，拉伸，效果如下![](https://gitlab.com/0505/fablab/uploads/b774e8f093e8e33ca5089d439d65546b/20200403152239.png)
* 后来改成圆柱形![](https://gitlab.com/0505/fablab/uploads/42f35ddbb922582c9a940d3159eda1d5/20200403153155.png)
* 定位的方式是在外围圆柱上拉一根线然后画圆![](https://gitlab.com/0505/fablab/uploads/0fe5c171780612ac9812bc8eb8bd9099/20200403153412.png)
* 再依据中线偏移![](https://gitlab.com/0505/fablab/uploads/0e3273af29747cc55b25b07139e531f6/20200403153455.png)
* 剪切掉多余的线条，包括中线参考![](https://gitlab.com/0505/fablab/uploads/5099719812b6320bafdf4f27b10e0f0a/20200403153530.png)
* 最终结果如下<iframe src="https://nexpcb3.autodesk360.com/shares/public/SH919a0QTf3c32634dcf7865b6d004b5f0b9?mode=embed" width="1024" height="768" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>
  

